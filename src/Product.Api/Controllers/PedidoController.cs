﻿using Product.Application.Commands.PedidoCommands.AlterarPedidoCommand;
using Product.Application.Commands.PedidoCommands.CriarPedidoCommand;
using Product.Application.Queries.PedidoQueries.ObterPedidoByIdQuery;
using Product.Core.Mvc;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Product.Api.Controllers;

[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class PedidoController : BaseController
{
    private readonly IMediator _mediator;

    public PedidoController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<IActionResult> CriarPedido([FromBody] CriarPedidoCommandInput input)
    {
        return HandleResult(await _mediator.Send(input));
    }

    [HttpPut]
    public async Task<IActionResult> AlterarPedido([FromBody] AlterarPedidoCommandInput input)
    {
        return HandleResult(await _mediator.Send(input));
    }

    [HttpGet("obter-por-id")]
    public async Task<IActionResult> ObterPedidoPorId([FromQuery] ObterByIdQueryInput input)
    {
        return HandleResult(await _mediator.Send(input));
    }   
}