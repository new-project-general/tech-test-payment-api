﻿using Product.Application;
using Product.Core;
using Product.DI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Serilog;

namespace Product.Api.Extensions;

public static class ApiExtensions
{
    public static IServiceCollection AddApiServicesConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddControllers().AddNewtonsoftJson(opt =>
        {
            opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            opt.SerializerSettings.Converters.Add(new StringEnumConverter());
        });

        services
           .AddSwaggerConfiguration()
           .AddServices(configuration)
           .AddCqrs(typeof(IAssemblyMarker).Assembly);

        services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

        services.AddLogging(logging => logging.AddSerilog());

        return services;
    }

    public static WebApplication UseApiConfiguration(this WebApplication app)
    {
        if (app.Environment.IsDevelopmentOrInternal())
        {
            app.UseDeveloperExceptionPage();

        }

        app.UseRouting();

        app.UseHttpsRedirection();

        app.UseSwaggerConfiguration();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });

        return app;
    }

    public static bool IsDevelopmentOrInternal(this IWebHostEnvironment environment)
    {
        return environment.IsDevelopment() || environment.EnvironmentName == "Internal";
    }
}
