﻿using AutoMapper;
using Product.Application.Commands.PedidoCommands.CriarPedidoCommand;
using Product.Domain.Entidades;

namespace Product.Application.AutoMapper;

public class AutoMapperConfig : Profile
{
    public AutoMapperConfig()
    {
        CreateMap<CriarPedidoCommandInput, Pedido>()
               .ForMember(dest => dest.StatusPagamento, opt => opt.Ignore());

        CreateMap<Queries.PedidoQueries.ObterPedidoByIdQuery.ObterByIdQueryItem, Pedido>();

        CreateMap<VendedorDto, Vendedor>();
        CreateMap<PedidoItemDto, PedidoItem>();

    }
}
