﻿using Product.Core.Mediator.Commands;
using Product.Core.Notifications;
using Product.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;

namespace Product.Application.Commands.PedidoCommands.AlterarPedidoCommand;

public class AlterarPedidoCommandHandler : ICommandHandler<AlterarPedidoCommandInput, AlterarPedidoCommandResult>
{
    private readonly ILogger<AlterarPedidoCommandHandler> _logger;
    private readonly IPedidoRepository _pedidoRepository;
    private readonly INotifier _notifier;

    public AlterarPedidoCommandHandler(
        ILogger<AlterarPedidoCommandHandler> logger,
        IPedidoRepository PedidoRepository,
        INotifier notifier)
    {
        _logger = logger;
        _pedidoRepository = PedidoRepository;
        _notifier = notifier;
    }

    public async Task<AlterarPedidoCommandResult> Handle(AlterarPedidoCommandInput request, CancellationToken cancellationToken)
    {
        var result = await _pedidoRepository.Update(request.Status, request.PedidoId);
        if (!result)
            _notifier.Notify("Não foi possivel efeturar a alteração do status do pedido.");

        return new AlterarPedidoCommandResult();
    }

}
