﻿using Product.Core.Mediator.Commands;

namespace Product.Application.Commands.PedidoCommands.AlterarPedidoCommand;

public class AlterarPedidoCommandResult : CommandResult
{
    public Guid Id { get; set; }
}