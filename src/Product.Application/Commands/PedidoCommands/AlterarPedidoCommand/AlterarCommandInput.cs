﻿using Product.Core.Mediator.Commands;
using Product.Domain.Enums;

namespace Product.Application.Commands.PedidoCommands.AlterarPedidoCommand;

public class AlterarPedidoCommandInput : CommandInput<AlterarPedidoCommandResult>
{
    public Guid PedidoId { get; set; }   
    public StatusPagamento Status { get; set; }
}