﻿using Product.Core.Mediator.Commands;

namespace Product.Application.Commands.PedidoCommands.CriarPedidoCommand;

public class CriarPedidoCommandResult : CommandResult
{
    public Guid Id { get; set; }
}