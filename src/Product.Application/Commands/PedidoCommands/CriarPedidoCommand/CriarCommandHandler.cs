﻿using Product.Core.Mediator.Commands;
using Product.Core.Notifications;
using Product.Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Product.Domain.Entidades;
using DocumentFormat.OpenXml.Office2010.Excel;
using System.Reflection.Metadata.Ecma335;

namespace Product.Application.Commands.PedidoCommands.CriarPedidoCommand;

public class CriarPedidoCommandHandler : ICommandHandler<CriarPedidoCommandInput, CriarPedidoCommandResult>
{
    private readonly ILogger<CriarPedidoCommandHandler> _logger;
    private readonly IPedidoRepository _pedidoRepository;
    private readonly INotifier _notifier;
    private readonly IMapper _mapper;

    public CriarPedidoCommandHandler(
        ILogger<CriarPedidoCommandHandler> logger,
        IPedidoRepository PedidoRepository,
        INotifier notifier, IMapper mapper)
    {
        _logger = logger;
        _pedidoRepository = PedidoRepository;
        _notifier = notifier;
        _mapper = mapper;
    }

    public async Task<CriarPedidoCommandResult> Handle(CriarPedidoCommandInput request, CancellationToken cancellationToken)
    {
        var result = _pedidoRepository.Add(_mapper.Map<Pedido>(request));
        return new CriarPedidoCommandResult() { Id = result.Result };
    }
}

