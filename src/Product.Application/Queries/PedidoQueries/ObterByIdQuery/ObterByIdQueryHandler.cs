﻿using AutoMapper;
using Product.Core.Mediator.Commands;
using Product.Domain.Interfaces.Repositories;
using Product.Core.Notifications;

namespace Product.Application.Queries.PedidoQueries.ObterPedidoByIdQuery;

public class ObterByIdQueryHandler : ICommandHandler<ObterByIdQueryInput, ObterByIdQueryItem>
{
    private readonly IPedidoRepository _pedidoRepository;
    private readonly IMapper _mapper;
    private readonly INotifier _notifier;
    public ObterByIdQueryHandler(IPedidoRepository PedidoRepository, IMapper mapper, INotifier notifier)
    {
        _pedidoRepository = PedidoRepository;
        _mapper = mapper;
        _notifier = notifier;
    }

    public async Task<ObterByIdQueryItem> Handle(ObterByIdQueryInput request, CancellationToken cancellationToken)
    {
        var result = _mapper.Map<ObterByIdQueryItem>(_pedidoRepository.GetByIdAsync(request.IdPedido));

        if(result == null)        
            _notifier.Notify("Não foi possivel localizar o pedido");

        return result;
    }
}

