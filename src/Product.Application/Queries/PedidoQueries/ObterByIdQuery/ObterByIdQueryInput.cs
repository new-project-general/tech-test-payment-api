﻿using Product.Core.Mediator.Commands;

namespace Product.Application.Queries.PedidoQueries.ObterPedidoByIdQuery;

public class ObterByIdQueryInput : CommandInput<ObterByIdQueryItem>
{
    public Guid IdPedido { get; set; }

}