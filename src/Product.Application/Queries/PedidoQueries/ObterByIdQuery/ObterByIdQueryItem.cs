﻿using Product.Core.Mediator.Commands;

namespace Product.Application.Queries.PedidoQueries.ObterPedidoByIdQuery;

public class ObterByIdQueryItem : CommandResult
{
    public Guid PedidoId { get; set; }
    public string Descricao { get; set; }

    public VendedorDto Vendedor { get; set; }

    public List<PedidoItemDto> Itens { get; set; }
}

public class VendedorDto
{
    public Guid VendedorId { get; set; }
    public string Cpf { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Telefone { get; set; }
}

public class PedidoItemDto
{
    public Guid ItemId { get; set; }
    public string Descricao { get; set; }
    public decimal Quantidade { get; set; }
    public decimal Valor { get; set; }
    public string Marca { get; set; }
}