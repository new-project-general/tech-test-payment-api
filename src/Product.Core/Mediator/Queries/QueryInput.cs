﻿namespace Product.Core.Mediator.Queries;

public class QueryInput<TItem> : MediatorInput<TItem>
    where TItem : QueryResult
{
}