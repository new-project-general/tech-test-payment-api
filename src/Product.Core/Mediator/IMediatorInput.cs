﻿using MediatR;

namespace Product.Core.Mediator;

public interface IMediatorInput<out TMediatorResult>
    : IRequest<TMediatorResult> where TMediatorResult : IMediatorResult
{

}