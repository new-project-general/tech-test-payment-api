﻿namespace Product.Core.Mediator;

public class MediatorInput<TMediatorResult> : IMediatorInput<TMediatorResult>
    where TMediatorResult : IMediatorResult
{
}