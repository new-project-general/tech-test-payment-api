﻿using Product.Application.AutoMapper;
using Product.Domain.Interfaces.Repositories;
using Product.Infra.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;

namespace Product.DI;

public static class DependencyInjector
{
    public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddHttpContextAccessor()
            .AddRepositories()
            .AddAutoMapper(typeof(AutoMapperConfig))
            .AddHttpClient();

        return services;
    }

    private static IServiceCollection AddRepositories(this IServiceCollection services)
    {
        services.AddScoped<IPedidoRepository, PedidoRepository>();

        return services;
    }

}
