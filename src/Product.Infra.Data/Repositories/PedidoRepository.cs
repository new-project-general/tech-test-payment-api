﻿using AutoMapper;
using Product.Domain.Entidades;
using Product.Domain.Enums;
using Product.Domain.Interfaces.Repositories;

namespace Product.Infra.Data.Repositories;
public class PedidoRepository : IPedidoRepository
{
    private readonly List<Pedido> _pedidosEmMemoria = new();

    private readonly IMapper _mapper;
    public PedidoRepository(IMapper mapper)
    {
        _mapper = mapper;
    }

    public Task<Guid> Add(Pedido pedido)
    {
        _pedidosEmMemoria.Add(_mapper.Map<Pedido>(pedido));       
        return Task.FromResult(pedido.Id);
    }

    public Task<Pedido> GetByIdAsync(Guid id)
    {
        var pedido = _pedidosEmMemoria.FirstOrDefault(p => p.Id == id);
        return Task.FromResult(pedido);
    }

    public Task<bool> Update(StatusPagamento statusPagamento, Guid id)
    {
        var exiPedido = _pedidosEmMemoria.FirstOrDefault(p => p.Id == id);

        if (exiPedido != null)
        {
            exiPedido.StatusPagamento = statusPagamento;
            return Task.FromResult(true);
        }

        return Task.FromResult(false);
    }
}
