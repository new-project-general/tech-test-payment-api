﻿using Product.Domain.Entidades.Base;

namespace Product.Domain.Entidades
{
    public class Vendedor : Entidade
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
