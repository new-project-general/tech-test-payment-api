﻿using Product.Domain.Entidades.Base;
using Product.Domain.Enums;

namespace Product.Domain.Entidades;

public class Pedido : Entidade
{
    public string Descricao { get; set; }

    public Vendedor Vendedor { get; set; }

    public List<PedidoItem> Itens { get; set; }

    public StatusPagamento StatusPagamento { get; set; } = StatusPagamento.AguardandoPagamento;
}
