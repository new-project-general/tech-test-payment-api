﻿using Product.Domain.Entidades.Base;

namespace Product.Domain.Entidades
{
    public class PedidoItem : Entidade
    {
        public string  Descricao { get; set; }
        public decimal  Quantidade { get; set; }
        public decimal  Valor { get; set; }
        public string Marca { get; set; }
    }
}
