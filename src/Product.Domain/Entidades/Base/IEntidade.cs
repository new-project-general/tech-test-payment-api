﻿namespace Product.Domain.Entidades.Base;

public interface IEntidade
{
    public Guid Id { get; set; }    

}