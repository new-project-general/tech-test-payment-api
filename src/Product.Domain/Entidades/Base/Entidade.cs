﻿using Product.Domain.Enums;

namespace Product.Domain.Entidades.Base;

public abstract class Entidade : IEntidade
{
    protected Entidade()
    {
        Id = Guid.NewGuid();
        DataCriacao = DateTime.UtcNow;
    }
    public Guid Id { get; set; }
    public DateTime DataCriacao { get; private set; }
    public DateTime? DataAlteracao { get; set; }  

}
