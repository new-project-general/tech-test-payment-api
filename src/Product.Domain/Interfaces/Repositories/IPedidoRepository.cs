﻿using Product.Domain.Entidades;
using Product.Domain.Enums;

namespace Product.Domain.Interfaces.Repositories;

public interface IPedidoRepository
{
    public Task<Guid> Add(Pedido pedido);
    Task<Pedido> GetByIdAsync(Guid id);
    Task<bool> Update(StatusPagamento statusPagamento, Guid id);
}
