﻿namespace Product.Domain.Enums;

public enum StatusPagamento
{
    AguardandoPagamento =0,
    PagamentoAprovado = 1,
    EnviadoTransportadora = 2,
    Entregue = 3,
    Cancelada = 4,
}